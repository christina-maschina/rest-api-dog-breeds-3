package hr.tvz.vidakovic.android.mvp.controller;

import hr.tvz.vidakovic.android.mvp.service.DogService;
import hr.tvz.vidakovic.android.mvp.transfer.DogDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("dogs")
public class DogController {

    private final DogService dogService;

    public DogController(DogService dogService) {
        this.dogService = dogService;
    }

    @GetMapping
    public List<DogDTO> getAllDogs() {
        return dogService.getAllDogs();
    }

    @GetMapping("/{id}")
    public ResponseEntity<DogDTO> getById(@PathVariable final Integer id) {
        return dogService.findById(Long.valueOf(id))
                .map(dogDTO -> ResponseEntity
                        .status(HttpStatus.OK)
                        .body(dogDTO))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
