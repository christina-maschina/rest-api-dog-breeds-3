package hr.tvz.vidakovic.android.mvp.service;

import hr.tvz.vidakovic.android.mvp.transfer.DogDTO;

import java.util.List;
import java.util.Optional;

public interface DogService {
    List<DogDTO> getAllDogs();

    Optional<DogDTO> findById(Long valueOf);
}
