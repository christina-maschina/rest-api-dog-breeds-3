package hr.tvz.vidakovic.android.mvp.service;

import hr.tvz.vidakovic.android.mvp.model.Dog;
import hr.tvz.vidakovic.android.mvp.repository.DogRepository;
import hr.tvz.vidakovic.android.mvp.transfer.DogDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DogServiceImpl implements DogService {

    private final DogRepository dogRepo;
    private final ModelMapper modelMapper;

    public DogServiceImpl(DogRepository dogRepo, ModelMapper modelMapper) {
        this.dogRepo = dogRepo;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<DogDTO> getAllDogs() {
        return dogRepo.findAll()
                .stream()
                .map(dog -> modelMapper.map(dog, DogDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<DogDTO> findById(Long id) {
        Optional<Dog> dogOptional=dogRepo.findById(id);
        if (dogOptional.isEmpty()){
            return Optional.ofNullable(null);
        }
        DogDTO dogDTO=modelMapper.map(dogOptional.get(), DogDTO.class);
        return Optional.of(dogDTO);
    }
}
