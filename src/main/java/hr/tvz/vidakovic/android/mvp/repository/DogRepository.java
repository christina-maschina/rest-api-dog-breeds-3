package hr.tvz.vidakovic.android.mvp.repository;

import hr.tvz.vidakovic.android.mvp.model.Dog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DogRepository extends JpaRepository<Dog, Long> {
    List<Dog> findAll();
    Optional<Dog> findById(Long id);
}
