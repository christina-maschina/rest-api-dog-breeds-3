package hr.tvz.vidakovic.android.mvp.transfer;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@NoArgsConstructor
@Data
public class DogDTO implements Serializable {
    private String name;
    private String description;
    private String pathImage;
}
