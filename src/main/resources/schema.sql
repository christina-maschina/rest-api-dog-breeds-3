CREATE TABLE IF NOT EXISTS dog (
    id identity,
    name varchar (50) NOT NULL,
    description varchar (500) NOT NULL,
    path_image varchar (150) NOT NULL
);